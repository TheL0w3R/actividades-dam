package actividad1ut2;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 06/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        int a = 5;
        int b = 67;
        System.out.println("Valor de a: " + a + "\nValor de b: " + b);

        int aux = a;
        a = b;
        b = aux;
        System.out.println("Valor de a: " + a + "\nValor de b: " + b);
    }
}
