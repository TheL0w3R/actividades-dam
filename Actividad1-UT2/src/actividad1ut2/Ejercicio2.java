package actividad1ut2;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 06/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio2 extends MenuOption {

    public Ejercicio2() {
        super("Ejercicio 2");
    }

    @Override
    public void run() {
        System.out.print("Escribe un numero: ");
        char c = (char)sc.nextInt();

        System.out.println("El caracter ASCII correspondiente es: " + c);
    }
}
