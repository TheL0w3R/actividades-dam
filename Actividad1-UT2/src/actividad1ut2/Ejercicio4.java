package actividad1ut2;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 06/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio4 extends MenuOption {

    public Ejercicio4() {
        super("Ejercicio 4");
    }

    @Override
    public void run() {
        System.out.print("Introduce la edad del alumno: ");
        int edad = sc.nextInt();

        System.out.print(" A - Acceso directo\n G - Grado medio\n P - Prueba de acceso\nIntroduce una modalidad válida: ");
        String mod = sc.next();

        if(edad < 18 && mod.equalsIgnoreCase("A"))
            System.out.println("Pase por secretaría.");
        else if(edad >= 18 && (mod.equalsIgnoreCase("G") || mod.equalsIgnoreCase("P")))
            System.out.println("Solicite código en jefatura");
        else
            System.out.println("Las clases son en la R03, gracias");
    }
}
