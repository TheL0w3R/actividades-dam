package actividad1ut2;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 06/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio3 extends MenuOption {

    public Ejercicio3() {
        super("Ejercicio 3");
    }

    @Override
    public void run() {
        final double sal = 655.20;

        System.out.println("Salario mínimo interprofesional de 2016: " + sal);
        System.out.println("El próximo año se planifica una subida del 0,25%: " + (sal + ((sal*0.25)/100)));
    }
}
